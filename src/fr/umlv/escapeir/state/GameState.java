package fr.umlv.escapeir.state;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import fr.umlv.escapeir.EscapeIR;
import fr.umlv.escapeir.gesture.GestureRecognizer;
import fr.umlv.escapeir.level.AbstractLevel;
import fr.umlv.escapeir.level.Earth;
import fr.umlv.zen2.ApplicationContext;
import fr.umlv.zen2.ApplicationRenderCode;

/**
 * Game state handler
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 */
public class GameState extends AbstractState {
	private static AbstractLevel level;
	private BufferedImage bi;

	/**
	 * {@link AbstractState#AbstractState(ApplicationContext)}
	 */
	public GameState(ApplicationContext context) {
		super(context);
		this.bi = new BufferedImage(EscapeIR.WIDTH, EscapeIR.HEIGHT, BufferedImage.TYPE_INT_RGB);
		GameState.level = new Earth();
	}

	public static void setLevel(AbstractLevel level) {
		GameState.level = level;
	}

	/**
	 * Updates the gestures and the level
	 * {@link AbstractState#update(double)}
	 */
	@Override
	public void update(double delta) {
		GestureRecognizer.getInstance().update(this.getContext());
		level.update(delta);
	}

	/**
	 * Render the gestures and the level
	 * {@link AbstractState#render()}
	 */
	@Override
	public void render() {
		this.getContext().render(new ApplicationRenderCode() {
			@Override
			public void render(Graphics2D graphics) {
				Graphics2D db = (Graphics2D) bi.getGraphics();
				db.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				db.setColor(Color.BLACK);
	            db.fill(new Rectangle(0, 0, EscapeIR.WIDTH, EscapeIR.HEIGHT));
				level.render(db);
	            GestureRecognizer.getInstance().render(db);
	            graphics.drawImage(bi, 0, 0, null);
			}
		});
	}
}
