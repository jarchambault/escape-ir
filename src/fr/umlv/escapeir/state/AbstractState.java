package fr.umlv.escapeir.state;

import fr.umlv.escapeir.utils.Updatable;
import fr.umlv.zen2.ApplicationContext;

/**
 * Defines a default structure for the states
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 */
public abstract class AbstractState implements Updatable {
	private final ApplicationContext context;

	/**
	 * Constructor
	 * @param context The context to handle events
	 */
	public AbstractState(ApplicationContext context) {
		this.context = context;
	}

	/**
	 * Returns the context
	 * @return the context
	 */
	protected ApplicationContext getContext() {
		return this.context;
	}

	/**
	 * Update the logic of the state
	 * @param delta The delta time to handle elapsed times in animations
	 */
	public abstract void update(double delta);

	/**
	 * Abstract method to handle graphics
	 */
	public abstract void render();
}
