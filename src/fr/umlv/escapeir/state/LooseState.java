package fr.umlv.escapeir.state;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import fr.umlv.escapeir.EscapeIR;
import fr.umlv.zen2.ApplicationContext;
import fr.umlv.zen2.ApplicationRenderCode;

public class LooseState extends AbstractState {

	public LooseState(ApplicationContext context) {
		super(context);
	}

	@Override
	public void update(double delta) {
	}

	@Override
	public void render() {
		this.getContext().render(new ApplicationRenderCode() {
			@Override
			public void render(Graphics2D graphics) {
	            graphics.setColor(Color.BLACK);
	            graphics.fill(new Rectangle(0, 0, EscapeIR.WIDTH, EscapeIR.HEIGHT));
	            
	            graphics.setColor(Color.GREEN);
	            graphics.drawString("You loose", EscapeIR.WIDTH / 2, EscapeIR.HEIGHT / 2);
			}
		});
	}

}
