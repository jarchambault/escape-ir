package fr.umlv.escapeir.level;

import fr.umlv.escapeir.model.element.BossShip;
import fr.umlv.escapeir.model.element.EnnemyShipFactory.Difficulty;
import fr.umlv.escapeir.model.weapon.MissileWeapon;
import fr.umlv.escapeir.state.GameState;

public class Moon extends AbstractLevel {
	public Moon() {
		super("moon", Difficulty.MEDIUM);
	}

	@Override
	public void update(double delta) {
		super.update(delta);
		if (this.getShips() == 0)
			this.setBoss(new BossShip(75, MissileWeapon.ID));
		if (this.getBoss() != null && !this.getBoss().isAlive())
			GameState.setLevel(new Jupiter());
	}
}
