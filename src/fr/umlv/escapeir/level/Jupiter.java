package fr.umlv.escapeir.level;

import fr.umlv.escapeir.EscapeIR;
import fr.umlv.escapeir.EscapeIR.StateID;
import fr.umlv.escapeir.model.element.BossShip;
import fr.umlv.escapeir.model.element.EnnemyShipFactory.Difficulty;
import fr.umlv.escapeir.model.weapon.MissileWeapon;

public class Jupiter extends AbstractLevel {
	public Jupiter() {
		super("jupiter", Difficulty.HARD);
	}

	@Override
	public void update(double delta) {
		super.update(delta);
		if (this.getShips() == 0)
			this.setBoss(new BossShip(100, MissileWeapon.ID));
		if (this.getBoss() != null && !this.getBoss().isAlive())
			EscapeIR.stateSwitcher(StateID.WIN);
	}
}
