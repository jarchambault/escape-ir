package fr.umlv.escapeir.level;

import fr.umlv.escapeir.model.element.BossShip;
import fr.umlv.escapeir.model.element.EnnemyShipFactory.Difficulty;
import fr.umlv.escapeir.model.weapon.MissileWeapon;
import fr.umlv.escapeir.state.GameState;

public class Earth extends AbstractLevel {
	public Earth() {
		super("earth", Difficulty.EASY);
	}

	@Override
	public void update(double delta) {
		super.update(delta);
		if (this.getShips() == 0)
			this.setBoss(new BossShip(50, MissileWeapon.ID));
		if (this.getBoss() != null && !this.getBoss().isAlive())
			GameState.setLevel(new Moon());
	}
}
