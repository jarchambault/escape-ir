package fr.umlv.escapeir.utils;

/**
 * Defines an interface for Updatable objects (e.g. calculus on models before rendering)
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public interface Updatable {
	/**
	 * 
	 * @param delta
	 */
	public void update(double delta);
}
