package fr.umlv.escapeir.utils;

import java.awt.Graphics2D;

/**
 * Define an interface for Renderable objects (i.e. drawable objects)
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public interface Renderable {
	/**
	 * @param graphics the graphics manager
	 */
	public void render(Graphics2D graphics);
}
