package fr.umlv.escapeir.utils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

import javax.imageio.ImageIO;

/**
 * Handle images loading
 * Store the image in a hashmap and retrieve them if they are already loaded
 * This class is static
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class ImageStore {
	private ImageStore() {
	}
	
	private final static HashMap<String, BufferedImage> store = new HashMap<String, BufferedImage>(); 
	
	/**
	 * Returns a bufferedImage stored in resources/
	 * @param path the image to load from resources/ directory
	 * @return the BufferedImage
	 * @throws IOException
	 */
	public static BufferedImage getImage(String path) throws IOException {
		Objects.requireNonNull(path);
		BufferedImage image = store.get(path);
		if (image == null) {
			image = ImageIO.read(ImageStore.class.getClass().getResource("/resources/" + path));
			store.put(path, Objects.requireNonNull(image));
		}
		return image;
	}
}
