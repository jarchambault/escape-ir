package fr.umlv.escapeir.menu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

import fr.umlv.escapeir.utils.Renderable;
import fr.umlv.escapeir.utils.Updatable;

/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 * This class represents the menu in which the player 
 */
public class Menu implements Updatable, Renderable{
	private static int life = 0;
	private static int missile = 0;
	private static int fire = 0;
	private static int shiboolet = 0;
	
	
	public static void setLife(int life) {
		Menu.life = life;
	}

	public static void setMissile(int missile) {
		Menu.missile = missile;
	}

	public static void setFire(int fire) {
		Menu.fire = fire;
	}

	public static void setShiboolet(int shiboolet) {
		Menu.shiboolet = shiboolet;
	}

	public Menu() {

	}
	
	@Override
	public void render(Graphics2D graphics) {
		Point positionLife = new Point(0, 0);
		graphics.setColor(Color.BLACK);
		graphics.fill(new Rectangle(positionLife, new Dimension(100, 20)));
		graphics.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		graphics.setColor(Color.WHITE);
		graphics.drawString("Life " + getLife(), 5, 15);
		
		graphics.setColor(Color.BLACK);
		Point positionWM = new Point(120, 0);
		graphics.fill(new Rectangle(positionWM, new Dimension(20, 20)));
		graphics.setColor(Color.WHITE);
		graphics.drawString("M " + missile, 120, 15);
		
		graphics.setColor(Color.BLACK);
		Point positionWF = new Point(190, 0);
		graphics.fill(new Rectangle(positionWF, new Dimension(20, 20)));
		graphics.setColor(Color.WHITE);
		graphics.drawString("F " + fire, 190, 15);
		
		graphics.setColor(Color.BLACK);
		Point positionWS = new Point(260, 0);
		graphics.fill(new Rectangle(positionWS, new Dimension(20, 20)));
		graphics.setColor(Color.WHITE);
		graphics.drawString("S " + shiboolet, 260, 15);
		
	}

	@Override
	public void update(double delta) {
		
	}

	public static int getLife() {
		return life;
	}
}
