package fr.umlv.escapeir.gesture;

import java.awt.Point;
import java.util.ArrayList;

import fr.umlv.escapeir.EscapeIR;
import fr.umlv.escapeir.gesture.GestureRecognizer.GestureID;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public abstract class Gesture {
	private final GestureID id;
	protected static final double shakiness = (int) (EscapeIR.DIAGONALE * 2) / 100;

	/**
	 * Enumeration of the different orientation of a gesture
	 */
	
	public enum Orientation {
		UNKNOWN,
		NORTH,
		SOUTH,
		EAST,
		WEST,
		NORTH_WEST,
		NORTH_EAST,
		SOUTH_WEST,
		SOUTH_EAST,
	}

	public Gesture(GestureID id) {
		this.id = id;
	}

	public GestureID getId() {
		return this.id;
	}

	/**
	 * implements this gesture in a new Gesture class to recognized it.
	 * @param points
	 * @return true if the gesture is recognized.
	 */
	public abstract boolean recognized(ArrayList<Point> points);
	/**
	 * 
	 * @param point
	 * @param from
	 * 
	 * @return true if the orientation is south 
	 */
	public static boolean isSouth(Point point, Point from) {
		return Gesture.isSouth(point, from, false);
	}

	/**
	 * 
	 * @param reference
	 * @param point
	 * @return true if the point is not outside the tolerance zone of shakiness
	 */
	public static boolean isInShakinessZone(double reference, double point) {
		return point > (reference - Gesture.shakiness) && point < (reference + Gesture.shakiness);
	}
	/**
	 * 
	 * @param point
	 * @param from
	 * @param shakinessCompliant
	 * 
	 * @return true if the orientation is south and the point is not outside the tolerance zone of shakiness 
	 */
	
	public static boolean isSouth(Point point, Point from, boolean shakinessCompliant) {
		boolean isSouth = point.getY() > from.getY();
		if (!shakinessCompliant)
			return isSouth;

		return isSouth && Gesture.isInShakinessZone(from.getX(), point.getX());
	}
	/**
	 * @return true if the orientation is north
	 */
	public static boolean isNorth(Point point, Point from) {
		return Gesture.isNorth(point, from, false);
	}
	/**
	 * 
	 * @param point
	 * @param from
	 * @param shakinessCompliant
	 * 
	 * @return true if the orientation is north and the point is not outside the tolerance zone of shakiness
	 */
	public static boolean isNorth(Point point, Point from, boolean shakinessCompliant) {
		boolean isNorth = !Gesture.isSouth(point, from);
		if (!shakinessCompliant)
			return isNorth;
		
		return isNorth && Gesture.isInShakinessZone(from.getX(), point.getX());
	}
	/**
	 * 
	 * @param point
	 * @param from
	 * @return true if the orientation is West
	 */
	public static boolean isWest(Point point, Point from) {
		return point.getX() < from.getX();
	}
	/**
	 * 
	 * @param point
	 * @param from
	 * @param shakinessCompliant
	 * 
	 * @return true if the orientation is west the point is not outside the tolerance zone of shakiness
	 */
	public static boolean isWest(Point point, Point from, boolean shakinessCompliant) {
		boolean isWest = point.getX() < from.getX();
		if (!shakinessCompliant)
			return isWest;

		return isWest && Gesture.isInShakinessZone(from.getY(), point.getY());
	}

	public static boolean isEast(Point point, Point from) {
		return Gesture.isEast(point, from, false);
	}

	/**
	 * 
	 * @param point
	 * @param from
	 * @param shakinessCompliant
	 * 
	 * @return true if the orientation is east and the point is not outside the tolerance zone of shakiness
	 */
	public static boolean isEast(Point point, Point from, boolean shakinessCompliant) {
		boolean isEast = !Gesture.isWest(point, from, false);
		if (!shakinessCompliant)
			return isEast;
		
		return isEast && Gesture.isInShakinessZone(from.getY(), point.getY());
	}

	public static boolean isNorthWest(Point point, Point from) {
		return Gesture.isNorth(point, from) && Gesture.isWest(point, from);
	}

	public static boolean isNorthEast(Point point, Point from) {
		return Gesture.isNorth(point, from) && Gesture.isEast(point, from);
	}

	public static boolean isSouthWest(Point point, Point from) {
		return Gesture.isNorthEast(from, point);
	}

	public static boolean isSouthEast(Point point, Point from) {
		return Gesture.isNorthWest(from, point);
	}
	/**
	 * 
	 * @param from
	 * @param to
	 * @return the orientation of line beetween 2 points
	 */
	public static Orientation orientation(Point from, Point to) {
		if (isNorth(to, from, true))
			return Orientation.NORTH;
		if (isWest(to, from, true))
			return Orientation.WEST;
		if (isSouth(to, from, true))
			return Orientation.SOUTH;
		if (isEast(to, from, true))
			return Orientation.EAST;

		if (isNorthWest(to, from))
			return Orientation.NORTH_WEST;
		if (isSouthWest(to, from))
			return Orientation.SOUTH_WEST;
		if (isSouthEast(to, from))
			return Orientation.SOUTH_EAST;
		if (isNorthEast(to, from))
			return Orientation.NORTH_EAST;

		return Orientation.UNKNOWN;
	}
	/**
	 * 
	 * @param from
	 * @param to
	 * @return an int which represent the angle of line between 2 points
	 */
	public static int getAngle(Point from, Point to) {
		int angle = (int) Math.toDegrees(Math.atan2(to.getY() - from.getY(), to.getX() - from.getX()));
		return (angle < 0) ? angle + 360 : angle; 
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @return a double which represents the slope of a line between 2 points
	 */
	public static double slope(Point from, Point to) {
		return (to.getY() - from.getY()) / (to.getX() - from.getX());
	}
}
