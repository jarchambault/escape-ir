package fr.umlv.escapeir.gesture;

import java.awt.Point;
import java.util.ArrayList;
import fr.umlv.escapeir.gesture.GestureRecognizer.GestureID;
import fr.umlv.escapeir.level.AbstractLevel;

public class Shoot extends TranslationGesture{
	public Shoot() {
		super(GestureID.SHOOT);
	}

	@Override
	public boolean recognized(ArrayList<Point> points) {
		return super.recognized(points) && GestureRecognizer.getInstance().getStartingPoint().distance(AbstractLevel.getPosition()) < 20;
	}
}
