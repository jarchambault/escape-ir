package fr.umlv.escapeir.gesture;

import java.awt.Point;
import java.util.ArrayList;

import fr.umlv.escapeir.gesture.GestureRecognizer.GestureID;

public class LeftDrift extends TranslationGesture {
	public LeftDrift() {
		super(GestureID.LEFT_DRIFT);
	}

	@Override
	public boolean recognized(ArrayList<Point> points) {
		return super.recognized(points) && this.getOrientation() == Orientation.NORTH_WEST;
	}
}
