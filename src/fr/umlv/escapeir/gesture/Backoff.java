package fr.umlv.escapeir.gesture;

import java.awt.Point;
import java.util.ArrayList;

import fr.umlv.escapeir.gesture.GestureRecognizer.GestureID;

public class Backoff extends TranslationGesture {
	/**
	 * defines the ID of the gesture.
	 */
	public Backoff() {
		super(GestureID.BACKOFF);
	}
	/**
	 * Recognize a straight line from a top point to a bottom point.
	 */
	@Override
	public boolean recognized(ArrayList<Point> points) {
		return super.recognized(points) && this.getOrientation() == Orientation.SOUTH;
	}
}
