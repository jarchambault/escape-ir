package fr.umlv.escapeir.gesture;

import java.awt.Point;
import java.util.ArrayList;

import fr.umlv.escapeir.gesture.GestureRecognizer.GestureID;

public class LeftLooping extends CircleGesture {
	public LeftLooping() {
		super(GestureID.LEFT_LOOPING);
	}

	@Override
	public boolean recognized(ArrayList<Point> points) {
		if (points.size() < 4)
			return false;
		int multiple = (int) Math.floor((points.size() - 1) / 4);
		Point reference = points.get(0);
		Point p1 = points.get(multiple);
		Point p2 = points.get(multiple * 2);
		Point p3 = points.get(multiple * 3);
		return super.recognized(points) && CircleGesture.isCounterClockwise(reference, p1, p2, p3);
	}
}
