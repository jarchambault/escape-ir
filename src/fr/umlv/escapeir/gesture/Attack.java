package fr.umlv.escapeir.gesture;

import java.awt.Point;
import java.util.ArrayList;

import fr.umlv.escapeir.gesture.GestureRecognizer.GestureID;

/**
 * Represents an attack
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class Attack extends TranslationGesture {
	/**
	 * defines the ID of the gesture.
	 */
	public Attack() {
		super(GestureID.ATTACK);
	}
	
	/**
	 * Recognize a straight line from a bottom point to a top point.
	 */
	@Override
	public boolean recognized(ArrayList<Point> points) {
		return super.recognized(points) && this.getOrientation() == Orientation.NORTH;
	}
}
