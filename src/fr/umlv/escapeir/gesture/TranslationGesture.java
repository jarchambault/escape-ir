package fr.umlv.escapeir.gesture;

import java.awt.Point;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import fr.umlv.escapeir.gesture.GestureRecognizer.GestureID;

public abstract class TranslationGesture extends Gesture {
	private Orientation orientation;

	public TranslationGesture(GestureID id) {
		super(id);
	}

	public Orientation getOrientation() {
		return this.orientation;
	}

	@Override
	public boolean recognized(ArrayList<Point> points) {
		if (points.size() < 3)
			return false;

		final Point startingPoint = points.get(0);
		final Point endingPoint = points.get(points.size() - 1);
		this.orientation = Gesture.orientation(startingPoint, endingPoint);

		for (int i = 1 ; i < (points.size()-1) ; i++) {
			final double distance = new Line2D.Double(startingPoint, endingPoint).ptLineDist(points.get(i));
			if (distance > 10)
				return false;
		}

		return true;
	}
}
