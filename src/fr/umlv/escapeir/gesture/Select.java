package fr.umlv.escapeir.gesture;

import java.awt.Point;
import java.util.ArrayList;


import fr.umlv.escapeir.gesture.GestureRecognizer.GestureID;

public class Select extends Gesture{

	public enum Selected{
		MISSIL,
		FIRE,
		SHIBOOLET,
		UKNOWN,
	}
	
	public Select() {
		super(GestureID.SELECT);
	}

	@Override
	public boolean recognized(ArrayList<Point> points) {
		return GestureRecognizer.getInstance().getStartingPoint() ==  GestureRecognizer.getInstance().getEndingPoint();
	}
	
}
