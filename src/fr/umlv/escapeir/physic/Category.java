package fr.umlv.escapeir.physic;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public enum Category {
	WALL(0x0001),PLAYER_BULLET(0x0002),ENNEMY(0x0004),BOSS(0x0008),PLAYER(0x0016),ENNEMY_BULLET(0x0032);

	private final int value;
	
	private Category(int value){
		this.value = value;
	}
	public int getValue() {
		return this.value;
	}
}
