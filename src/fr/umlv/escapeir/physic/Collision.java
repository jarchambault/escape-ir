package fr.umlv.escapeir.physic;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.contacts.Contact;

import fr.umlv.escapeir.model.element.AbstractShip;
import fr.umlv.escapeir.model.element.Bullet;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class Collision implements ContactListener{

	@Override
	public void beginContact(Contact contact) {
		
		/**
		 * Test if a player bullet hit an ennemy
		 */
		if(contact.getFixtureA().getFilterData().categoryBits == Category.PLAYER_BULLET.getValue() && contact.getFixtureB().getFilterData().categoryBits == Category.ENNEMY.getValue()){
			AbstractShip ship = (AbstractShip) contact.getFixtureB().getBody().getUserData();
			Bullet bullet = (Bullet) contact.getFixtureA().getBody().getUserData();
			ship.isHit(bullet);
			bullet.hit();
		}
		if(contact.getFixtureB().getFilterData().categoryBits == Category.PLAYER_BULLET.getValue() && contact.getFixtureA().getFilterData().categoryBits == Category.ENNEMY.getValue()){
			AbstractShip ship = (AbstractShip) contact.getFixtureA().getBody().getUserData();
			Bullet bullet = (Bullet) contact.getFixtureB().getBody().getUserData();
			ship.isHit(bullet);
			bullet.hit();
		}
		
		
		/**
		 * Test if ennemy bullet hit the player
		 */
		if(contact.getFixtureB().getFilterData().categoryBits == Category.ENNEMY_BULLET.getValue() && contact.getFixtureA().getFilterData().categoryBits == Category.PLAYER.getValue()){
			AbstractShip ship = (AbstractShip) contact.getFixtureA().getBody().getUserData();
			Bullet bullet = (Bullet) contact.getFixtureB().getBody().getUserData();
			ship.isHit(bullet);
			bullet.hit();
		}
		if(contact.getFixtureB().getFilterData().categoryBits == Category.ENNEMY_BULLET.getValue() && contact.getFixtureA().getFilterData().categoryBits == Category.PLAYER.getValue()){
			AbstractShip ship = (AbstractShip) contact.getFixtureA().getBody().getUserData();
			Bullet bullet = (Bullet) contact.getFixtureB().getBody().getUserData();
			ship.isHit(bullet);
			bullet.hit();
		}
		
		/**
		 * Test if bullet hit a boss
		 */
		if(contact.getFixtureB().getFilterData().categoryBits == Category.PLAYER_BULLET.getValue() && contact.getFixtureA().getFilterData().categoryBits == Category.BOSS.getValue()){
			AbstractShip ship = (AbstractShip) contact.getFixtureA().getBody().getUserData();
			Bullet bullet = (Bullet) contact.getFixtureB().getBody().getUserData();
			ship.isHit(bullet);
			bullet.hit();
		}
		if(contact.getFixtureB().getFilterData().categoryBits == Category.PLAYER_BULLET.getValue() && contact.getFixtureA().getFilterData().categoryBits == Category.BOSS.getValue()){
			AbstractShip ship = (AbstractShip) contact.getFixtureA().getBody().getUserData();
			Bullet bullet = (Bullet) contact.getFixtureB().getBody().getUserData();
			ship.isHit(bullet);
			bullet.hit();
		}
		
		/**
		 * Test if ennemy hit the player
		 */
		if(contact.getFixtureB().getFilterData().categoryBits == Category.PLAYER.getValue() && contact.getFixtureA().getFilterData().categoryBits == Category.ENNEMY.getValue()){
			AbstractShip ship = (AbstractShip) contact.getFixtureA().getBody().getUserData();
			ship.isHit();
		}
		if(contact.getFixtureB().getFilterData().categoryBits == Category.PLAYER.getValue() && contact.getFixtureA().getFilterData().categoryBits == Category.ENNEMY.getValue()){
			AbstractShip ship = (AbstractShip) contact.getFixtureA().getBody().getUserData();
			ship.isHit();
		}
		
		/**
		 * Test if a boss hit the player
		 */
		if(contact.getFixtureB().getFilterData().categoryBits == Category.PLAYER.getValue() && contact.getFixtureA().getFilterData().categoryBits == Category.BOSS.getValue()){
			AbstractShip ship = (AbstractShip) contact.getFixtureA().getBody().getUserData();
			ship.isHit();
		}
		if(contact.getFixtureB().getFilterData().categoryBits == Category.PLAYER.getValue() && contact.getFixtureA().getFilterData().categoryBits == Category.BOSS.getValue()){
			AbstractShip ship = (AbstractShip) contact.getFixtureA().getBody().getUserData();
			ship.isHit();
		}
	}

	@Override
	public void endContact(Contact arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void postSolve(Contact arg0, ContactImpulse arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void preSolve(Contact arg0, Manifold arg1) {
		// TODO Auto-generated method stub

	}

}
