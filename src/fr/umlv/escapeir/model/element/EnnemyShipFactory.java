package fr.umlv.escapeir.model.element;

import java.util.ArrayList;
import java.util.Random;

import fr.umlv.escapeir.model.weapon.FireWeapon;
import fr.umlv.escapeir.model.weapon.MissileWeapon;
import fr.umlv.escapeir.model.weapon.ShiboleetWeapon;

/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public abstract class EnnemyShipFactory {
	
	/**
	 * 
	 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
	 *
	 */
	public enum Difficulty {
		EASY,
		MEDIUM,
		HARD,
	};

	public static String[] WEAPONS = {MissileWeapon.ID, FireWeapon.ID, ShiboleetWeapon.ID};

	private EnnemyShipFactory() {
		throw null;
	}

	public static EnnemyShip buildRandomShip(int life) {
		String ships[] = {"fire", "shiboleet", "missile"};
		Random randomer = new Random();
		EnnemyShip ship;

		switch (ships[randomer.nextInt(ships.length)]) {
			case "fire":
				ship = new FireShip(life);
				break;
			case "shiboleet":
				ship = new ShiboleetShip(life);
				break;
			default:
				ship = new MissileShip(life);
				break;
		}
		return ship;
	}

	public static ArrayList<EnnemyShip> buildShipArmy(Difficulty difficulty, int shipsNumber) {
		ArrayList<EnnemyShip> ships = new ArrayList<>();
		Random randomer = new Random();
		int life = 10;
		switch (difficulty) {
			case HARD:
				life *= 2;
			case MEDIUM:
				life *= 2;
				break;
			case EASY:
			default:
				break;
		}

		for (int i = 0 ; i < randomer.nextInt(shipsNumber+1) ; i++)
			ships.add(EnnemyShipFactory.buildRandomShip(life));
		
		return ships;
	}
}
