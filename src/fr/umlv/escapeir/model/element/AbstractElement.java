
package fr.umlv.escapeir.model.element;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

import fr.umlv.escapeir.gesture.GestureRecognizer;
import fr.umlv.escapeir.gesture.GestureRecognizer.GestureID;
import fr.umlv.escapeir.physic.Category;
import fr.umlv.escapeir.physic.World;
import fr.umlv.escapeir.utils.Renderable;
import fr.umlv.escapeir.utils.Updatable;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public abstract class AbstractElement implements Updatable, Renderable {
	private Body body;
	private Vec2 impulse;
	private int strength;
	private GestureID runningGesture;

	/**
	 * @param position
	 * @param life
	 * @param weapons
	 * @param weapon
	 */
	public AbstractElement(Vec2 position, int category, int mask) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DYNAMIC;
		bodyDef.position = position;
		
		PolygonShape polygonShape = new PolygonShape();
		polygonShape.setAsBox(1.1f, 1.1f);
		polygonShape.m_radius = 1;

		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = polygonShape;
		fixtureDef.density = 1;
		fixtureDef.friction = 0;
		fixtureDef.restitution = 0;
		fixtureDef.filter.categoryBits = category;
		fixtureDef.filter.maskBits = mask;
		
		this.setBody(World.WORLD.createBody(bodyDef));
		this.getBody().createFixture(fixtureDef);
		this.getBody().setLinearDamping(0.01f);
	}

	/**
	 * 
	 * @param position
	 */
	public AbstractElement(Vec2 position) {
		this(position, Category.ENNEMY.getValue(), Category.WALL.getValue()|Category.PLAYER_BULLET.getValue()|Category.PLAYER.getValue());
	}

	/**
	 * This method is used so the ship moves in function of the gesture's size.
	 * Just give it a Vec2 for the direction.
	 * @param impulse
	 */
	//Dirty Hack : to apply Impulse strength times
	protected void move(Vec2 impulse) {
		this.move(impulse, (int) (GestureRecognizer.getInstance().getDistance() / 20) + 1);
	}
	/**
	 * This method define the direction and the strength of an impulse.
	 * @param impulse
	 * @param strength
	 */
	public void move(Vec2 impulse, int strength) {
		this.impulse = impulse;
		this.strength = strength;
	}

	/**
	 * This method applies an impulse strength times. 
	 */
	public void move() {
		if (this.strength == 0) {
			if (this.runningGesture == GestureID.LEFT_LOOPING || this.runningGesture == GestureID.RIGHT_LOOPING) {
				this.runningGesture = null;
				this.move(new Vec2(-this.impulse.x, this.impulse.y), 10);
			}
			return;
		}
		this.getBody().applyLinearImpulse(this.impulse, this.getBody().getWorldCenter());
		this.strength--;
	}

	/**
	 * 
	 * @return the Body of an element
	 */
	public Body getBody() {
		return body;
	}
	/**
	 * This method allows you to change the body of an element.
	 * @param body
	 */
	public void setBody(Body body) {
		this.body = body;
	}

	public void setRunningGesture(GestureID runningGesture) {
		this.runningGesture = runningGesture;
	}
}
