package fr.umlv.escapeir.model.element;

import java.util.Random;

import org.jbox2d.common.Vec2;

import fr.umlv.escapeir.gesture.GestureRecognizer;
import fr.umlv.escapeir.level.AbstractLevel;
import fr.umlv.escapeir.physic.Category;
import fr.umlv.escapeir.physic.World;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public abstract class EnnemyShip extends AbstractShip {
	private static final int DELAY = 20;
	private int delayed = EnnemyShip.DELAY;

	public EnnemyShip(int life, String weapon, String spriteName) {
		super(new Vec2(new Random().nextInt(World.WIDTH), World.HEIGHT - 6), life, weapon, Category.ENNEMY.getValue(), Category.PLAYER_BULLET.getValue()|Category.WALL.getValue(), spriteName);
		this.getBody().setUserData(this);
	}

	public abstract Bullet shoot();

	protected Bullet shoot(Bullet bullet) {
		Vec2 vecPos = this.getBody().getPosition().clone();
		vecPos.y = vecPos.y - 5;
		AbstractLevel.getBullets().add(bullet);
		return bullet;
	}

	@Override
	public void update(double delta) {
		if (this.delayed > 0) {
			this.delayed--;
			return;
		}
		Random randomer = new Random();
		switch (GestureRecognizer.GestureID.values()[randomer.nextInt(4)]) {
			case BACKOFF:
				this.move(new Vec2(0, 1), randomer.nextInt(10)+1);
				break;
			case ATTACK:
				this.move(new Vec2(0, -1), randomer.nextInt(10)+1);
			case SHOOT:
				int x = randomer.nextInt(2);
				x = (randomer.nextInt(2) == 1) ? x : -x;
				Bullet bullet = this.shoot();
				if (bullet != null)
					bullet.move(new Vec2(x, -randomer.nextInt(10)), 100);
				this.shiboleetHack(new Vec2(x, -randomer.nextInt(10)));
				break;
			case LEFT_DRIFT:
				this.move(new Vec2(-10, 5), randomer.nextInt(10)+1);
				break;
			case RIGHT_DRIFT:
				this.move(new Vec2(10, 5), randomer.nextInt(10)+1);
				break;
			default:
				this.move(new Vec2(0, -1));
				break;
		}
		this.move();
		this.delayed = EnnemyShip.DELAY;
	}
}
