package fr.umlv.escapeir.model.element;

import fr.umlv.escapeir.model.weapon.ShiboleetWeapon;
import fr.umlv.escapeir.physic.Category;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class ShiboleetShip extends EnnemyShip {
	public ShiboleetShip(int life) {
		super(life, ShiboleetWeapon.ID, "shiboleetship.png");
	}

	@Override
	public Bullet shoot() {
		return super.shoot(new ShiboleetBullet(this.getBody().getPosition().clone(), Category.ENNEMY_BULLET.getValue(), Category.PLAYER.getValue()));
	}
}
