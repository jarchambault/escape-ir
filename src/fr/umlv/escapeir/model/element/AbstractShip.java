package fr.umlv.escapeir.model.element;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import org.jbox2d.common.Vec2;

import fr.umlv.escapeir.model.weapon.AbstractWeapon;
import fr.umlv.escapeir.model.weapon.FireWeapon;
import fr.umlv.escapeir.model.weapon.MissileWeapon;
import fr.umlv.escapeir.model.weapon.ShiboleetWeapon;
import fr.umlv.escapeir.physic.Category;
import fr.umlv.escapeir.physic.World;
import fr.umlv.escapeir.utils.ImageStore;

/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public abstract class AbstractShip extends AbstractElement{
	private int life;
	private final int initialLife;
	private final HashMap<String, AbstractWeapon> weapons;
	private String selectedWeapon;
	private BufferedImage sprite;
	
	public AbstractShip(Vec2 position, int life, String weapon, int category, int mask, String spriteName) {
		super(position, category, mask);
		this.setLife(life);
		this.initialLife = this.life;
		this.selectedWeapon = weapon;
		this.weapons = new HashMap<>();
		int categoryWeapon = Category.ENNEMY_BULLET.getValue();
		if (category == Category.PLAYER.getValue())
			categoryWeapon = Category.PLAYER_BULLET.getValue();
		this.weapons.put(MissileWeapon.ID, new MissileWeapon(categoryWeapon, 40));
		this.weapons.put(FireWeapon.ID, new FireWeapon(categoryWeapon, 20));
		this.weapons.put(ShiboleetWeapon.ID, new ShiboleetWeapon(categoryWeapon, 10));
		try {
			this.sprite = ImageStore.getImage(spriteName);
		} catch (IOException exception) {
			System.err.println("Unfindable image " + exception.getMessage());
			return;
		}
	}	
	/**
	 * 
	 * @return HashMap of weapons
	 */
	public HashMap<String, AbstractWeapon> getWeapons() {
		return weapons;
	}
	/**
	 * 
	 * @return the number of life of this ship
	 */
	public int getLife(){
		return this.life;
	}

	/**
	 * @param life
	 */
	
	public void setLife(int life) {
		this.life = life;
	}

	public int getInitialLife() {
		return this.initialLife;
	}
	/**
	 * 
	 * @return the weapon selected by the player.
	 */
	public AbstractWeapon getSelectedWeapon() {
		return this.weapons.get(this.selectedWeapon);
	}
	
	/**
	 * 
	 * @return true if the ship still have lives false another wise
	 */
	public boolean isAlive(){
		if(this.getLife() > 0)
			return true;
		return false;
	}

	/**
	 *  
	 */
	public void isHit(Bullet bullet){
		if(this.isAlive()){
			this.setLife(this.getLife() - bullet.getDamages());
		}
	}

	public void isHit(){
		if(this.isAlive()){
			this.setLife(this.getLife() - 1);
		}
	}

	/**
	 * This method changes the selected weapon of a ship.
	 * @param weapon
	 */
	public void changeWeapon(String weapon){
		if(this.getWeapons().get(weapon).isOutOfAmmo())
			throw new IllegalArgumentException();
		this.selectedWeapon = weapon;
	}
	/**
	 * Implements this method in a ship so it can shoot bullets
	 * @return a new bullet
	 */
	public abstract Bullet shoot();
	
	@Override
	public abstract void update(double delta);
	
	@Override
	public void render(Graphics2D graphics) {
		Point position = World.toPixel(this.getBody().getPosition());
		if(this.isAlive())
			graphics.setColor(Color.RED);
		graphics.drawImage(this.getSprite(), null, (int) (position.getX() - this.getSprite().getWidth() / 2), (int) (position.getY() - this.getSprite().getHeight() / 2));
	}

	public BufferedImage getSprite() {
		return this.sprite;
	}

	public void shiboleetHack(Vec2 direction) {
		if (this.getSelectedWeapon().getId() != ShiboleetWeapon.ID)
			return;
		Bullet bulletShoot2 = this.shoot();
		Bullet bulletShoot3 = this.shoot();
		if (bulletShoot2 != null) {
			bulletShoot2.move(new Vec2(direction.x - 5, direction.y), 100);
			this.getSelectedWeapon().addAmmo(1);
		}
		if (bulletShoot3 != null) {
			bulletShoot3.move(new Vec2(direction.x + 5, direction.y), 100);
			this.getSelectedWeapon().addAmmo(1);
		}
	}
}
