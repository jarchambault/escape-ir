package fr.umlv.escapeir.model.element;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;

import org.jbox2d.common.Vec2;

import fr.umlv.escapeir.physic.World;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class FireBullet extends Bullet {
	public FireBullet(Vec2 position, int category, int mask) {
		super(10, position, category, mask);
	}

	@Override
	public void render(Graphics2D graphics) {
		Point position = World.toPixel(this.getBody().getPosition());
		graphics.setColor(Color.ORANGE);
		graphics.fill(new Ellipse2D.Float((float) position.getX(), (float) position.getY(), 10, 10));
	}
}
