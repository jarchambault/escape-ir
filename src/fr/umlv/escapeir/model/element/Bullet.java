package fr.umlv.escapeir.model.element;

import org.jbox2d.common.Vec2;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public abstract class Bullet extends AbstractElement {
	private boolean hit;
	private int damages;

	/**
	 * You have to give a category and mask so the enemies ships do not shoot each other.
	 * @param position
	 * @param category
	 * @param mask
	 */
	public Bullet(int damages, Vec2 position, int category, int mask) {
		super(position, category, mask);
		this.getBody().setUserData(this);
		this.hit = false;
		this.damages = damages;
	}

	public int getDamages() {
		return this.damages;
	}

	@Override
	public void update(double delta) {
		this.move();
	}

	public void hit() {
		this.hit = true;
	}
	public boolean getHit(){
		return this.hit;
	}
}
