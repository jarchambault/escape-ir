package fr.umlv.escapeir.model.element;

import java.util.Random;

import org.jbox2d.common.Vec2;

import fr.umlv.escapeir.level.AbstractLevel;
import fr.umlv.escapeir.model.weapon.MissileWeapon;

/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */

public class BossShip extends EnnemyShip {
	public BossShip(int life, String weapon) {
		super(life, MissileWeapon.ID, "bossship.png");
	}

	@Override
	public void update(double delta) {
		super.update(delta);
		this.changeWeapon(EnnemyShipFactory.WEAPONS[new Random().nextInt(EnnemyShipFactory.WEAPONS.length)]);
	}

	@Override
	public Bullet shoot() {
		Vec2 vecPos = this.getBody().getPosition().clone();
		vecPos.y = vecPos.y - 5;

		Bullet bullet = this.getSelectedWeapon().shoot(vecPos);
		if (bullet != null)
			AbstractLevel.getBullets().add(bullet);
		return bullet;
	}
}

