package fr.umlv.escapeir.model.element;

import fr.umlv.escapeir.model.weapon.FireWeapon;
import fr.umlv.escapeir.physic.Category;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class FireShip extends EnnemyShip {
	public FireShip(int life) {
		super(life, FireWeapon.ID, "fireship.png");
	}

	@Override
	public Bullet shoot() {
		return super.shoot(new FireBullet(this.getBody().getPosition().clone(), Category.ENNEMY_BULLET.getValue(), Category.PLAYER.getValue()));
	}
}
