package fr.umlv.escapeir.model.element;

import java.awt.Point;

import org.jbox2d.common.Vec2;
import fr.umlv.escapeir.EscapeIR;
import fr.umlv.escapeir.gesture.GestureRecognizer;
import fr.umlv.escapeir.gesture.GestureRecognizer.GestureID;
import fr.umlv.escapeir.level.AbstractLevel;
import fr.umlv.escapeir.menu.Menu;
import fr.umlv.escapeir.model.weapon.FireWeapon;
import fr.umlv.escapeir.model.weapon.MissileWeapon;
import fr.umlv.escapeir.model.weapon.ShiboleetWeapon;
import fr.umlv.escapeir.physic.Category;
import fr.umlv.escapeir.physic.World;

public class PlayerShip extends AbstractShip {
	private static Vec2 INITIAL_POSITION = new Vec2(World.toWorld(new Point((int) (EscapeIR.WIDTH / 2), (int) (EscapeIR.HEIGHT * 9 / 10))));
	public PlayerShip(int life) {
		super(PlayerShip.INITIAL_POSITION, life, MissileWeapon.ID, Category.PLAYER.getValue(),
				Category.WALL.getValue()|Category.ENNEMY_BULLET.getValue()|Category.ENNEMY.getValue()|Category.BOSS.getValue(), "playership.gif");
		this.getBody().setUserData(this);
 	}

	@Override
	public Bullet shoot() {
		Vec2 vecPos = this.getBody().getPosition().clone();
		vecPos.y = vecPos.y +5;

		Bullet bullet = this.getSelectedWeapon().shoot(vecPos);
		if (bullet != null)
			AbstractLevel.getBullets().add(bullet);
		return bullet;
	}
	
	public void isHit(){
		if(this.isAlive()){
			this.setLife(this.getLife() - 1);
			Menu.setLife(Menu.getLife() - 1);
		}
	}
	/**
	 * Detect on which weapon the user just clicked
	 * 
	 */
	public void selectedWeapon(){
		if(GestureRecognizer.getInstance().getStartingPoint().distance(new Point(140, 10)) <= 10){
			this.changeWeapon(MissileWeapon.ID);
		}
		if(GestureRecognizer.getInstance().getStartingPoint().distance(new Point(200, 10)) <= 10){
			this.changeWeapon(FireWeapon.ID);
		}
		if(GestureRecognizer.getInstance().getStartingPoint().distance(new Point(270, 10)) <= 10){
			this.changeWeapon(ShiboleetWeapon.ID);
		}
	}
	
	@Override
	public void update(double delta) {
		Menu.setLife(this.getLife());
		Menu.setMissile(this.getWeapons().get(MissileWeapon.ID).getAmmo());
		Menu.setFire(this.getWeapons().get(FireWeapon.ID).getAmmo());
		Menu.setShiboolet(this.getWeapons().get(ShiboleetWeapon.ID).getAmmo());
		
		Vec2 direction = GestureRecognizer.getInstance().getAngle();
		switch (GestureRecognizer.getInstance().getGesture()) {
			case BACKOFF:
				this.move(new Vec2(0, -1));
				break;
			case ATTACK:
				this.move(new Vec2(0, 1));
			case SHOOT:
				System.out.println(direction);
				Bullet bulletShoot = this.shoot();
				if (bulletShoot != null)
					bulletShoot.move(direction, 100);
				this.shiboleetHack(direction);
				break;
			case LEFT_DRIFT:
				this.move(direction);
				break;
			case RIGHT_DRIFT:
				this.move(direction);
				break;
			case SELECT:
				this.selectedWeapon();
				break;
			case LEFT_LOOPING:
				this.setRunningGesture(GestureID.LEFT_LOOPING);
				this.move(new Vec2(-1, 0), 10);
				break;
			case RIGHT_LOOPING:
				this.setRunningGesture(GestureID.RIGHT_LOOPING);
				this.move(new Vec2(1, 0), 10);
				break;
			default:
				break;
		}
		this.move();
	}
}
