package fr.umlv.escapeir.model.element;

import fr.umlv.escapeir.model.weapon.MissileWeapon;
import fr.umlv.escapeir.physic.Category;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class MissileShip extends EnnemyShip {

	public MissileShip(int life) {
		super(life, MissileWeapon.ID, "missileship.png");
	}

	@Override
	public Bullet shoot() {
		return super.shoot(new MissileBullet(this.getBody().getPosition().clone(), Category.ENNEMY_BULLET.getValue(), Category.PLAYER.getValue()));
	}
}
