package fr.umlv.escapeir.model.weapon;

import org.jbox2d.common.Vec2;

import fr.umlv.escapeir.model.element.Bullet;
import fr.umlv.escapeir.model.element.MissileBullet;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class MissileWeapon extends AbstractWeapon {
	public static final String ID = "missile";

	public MissileWeapon(int category, int ammo) {
		super(MissileWeapon.ID, category, ammo);
	}

	@Override
	public Bullet shoot(Vec2 position) {
		return super.shoot(new MissileBullet(position, this.getCategory(), this.getMask()));
	}
}
