package fr.umlv.escapeir.model.weapon;

import org.jbox2d.common.Vec2;

import fr.umlv.escapeir.model.element.Bullet;
import fr.umlv.escapeir.physic.Category;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public abstract class AbstractWeapon {
	private int ammo;
	private final int category;
	private final int mask;
	private final String id;

	public AbstractWeapon(String id, int category, int ammo) {
		this.setAmmo(ammo);
		this.id = id;
		this.category = category;
		if (this.category == Category.ENNEMY_BULLET.getValue()) {
			this.mask = Category.PLAYER.getValue();
			this.ammo *= 10;
		}
		else
			this.mask = Category.BOSS.getValue()|Category.ENNEMY.getValue();
	}

	public String getId() {
		return this.id;
	}
	protected Bullet shoot(Bullet bullet) {
		if (this.isOutOfAmmo())
			return null;
		this.minusAmmo();
		return bullet;
	}

	public abstract Bullet shoot(Vec2 position);

	public int getAmmo() {
		return this.ammo;
	}

	public boolean isOutOfAmmo() {
		return this.getAmmo() == 0;
	}

	protected void setAmmo(int ammo) {
		if (ammo < 0)
			throw new IllegalArgumentException();
		this.ammo = ammo;
	}

	public void minusAmmo() {
		try {
			this.setAmmo(this.getAmmo() - 1);
		} catch (IllegalArgumentException exception) {
			this.setAmmo(0);
		}
	}

	public void addAmmo(int ammo) {
		try {
			this.setAmmo(this.getAmmo() + ammo);
		} catch (IllegalArgumentException e) {
			this.setAmmo(0);
		}
	}

	public int getCategory() {
		return this.category;
	}

	public int getMask() {
		return this.mask;
	}
}
