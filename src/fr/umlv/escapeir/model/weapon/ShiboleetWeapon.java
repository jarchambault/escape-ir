package fr.umlv.escapeir.model.weapon;

import org.jbox2d.common.Vec2;

import fr.umlv.escapeir.model.element.Bullet;
import fr.umlv.escapeir.model.element.ShiboleetBullet;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class ShiboleetWeapon extends AbstractWeapon {
	public static final String ID = "shiboleet";

	public ShiboleetWeapon(int category, int ammo) {
		super(ShiboleetWeapon.ID, category, ammo);
	}

	@Override
 	public Bullet shoot(Vec2 position) {
		return super.shoot(new ShiboleetBullet(position, this.getCategory(), this.getMask()));
	}

}
