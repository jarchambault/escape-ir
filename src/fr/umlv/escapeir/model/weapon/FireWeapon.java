package fr.umlv.escapeir.model.weapon;

import org.jbox2d.common.Vec2;

import fr.umlv.escapeir.model.element.Bullet;
import fr.umlv.escapeir.model.element.FireBullet;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class FireWeapon extends AbstractWeapon {
	public static final String ID = "fire";

	public FireWeapon(int category, int ammo) {
		super(FireWeapon.ID, category, ammo);
	}

	@Override
	public Bullet shoot(Vec2 position) {
		return super.shoot(new FireBullet(position, this.getCategory(), this.getMask()));
	}
}
