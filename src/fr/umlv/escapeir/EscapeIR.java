package fr.umlv.escapeir;

import java.awt.geom.Point2D;
import java.util.HashMap;

import fr.umlv.escapeir.gesture.GestureRecognizer;
import fr.umlv.escapeir.state.AbstractState;
import fr.umlv.escapeir.state.GameState;
import fr.umlv.escapeir.state.LooseState;
import fr.umlv.escapeir.state.WinState;
import fr.umlv.zen2.Application;
import fr.umlv.zen2.ApplicationCode;
import fr.umlv.zen2.ApplicationContext;

/**
 * 
 * Main class of the game
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 */
public class EscapeIR {
	/**
	 * Horizontal size of the window
	 */

	public static final int WIDTH = 800;
	/**
	 * Vertical size of the window
	 */
	public static final int HEIGHT = 600;

	/**
	 * Computed diagonale based on WIDTH & HEIGHT
	 */
	public static final int DIAGONALE = (int) (Point2D.distance(0, 0, EscapeIR.WIDTH, EscapeIR.HEIGHT));

	/**
	 * Number of frames per second
	 */
	public static final int FPS = 30;

	/**
	 * Title of the window
	 */
	public static final String TITLE = "EscapeIR";

	/**
	 * Enumeration to manage states 
	 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
	 *
	 */
	public enum StateID {
		/**
		 * Game state
		 */
		GAME,
		/**
		 * Win state
		 */
		WIN,
		/**
		 * Loose state
		 */
		LOOSE,
		/**
		 * Quit state
		 */
		QUIT,
	}

	/**
	 * Default state
	 */
	private static StateID currentState = EscapeIR.StateID.GAME;

	/**
	 * Main function
	 * @param args
	 */
	public static void main(String[] args) {
		Application.run(EscapeIR.TITLE, EscapeIR.WIDTH, EscapeIR.HEIGHT, new ApplicationCode() {
			/**
			 * Associates identifiers with states
			 */
			private HashMap<StateID, AbstractState> states = new HashMap<>();

			/**
			 * Main loop
			 */
			@Override
			public void run(ApplicationContext context) {
				// Declare variables to handle sleep times after each loop according to the numbers of FPS
				long startingTime = System.currentTimeMillis();
				long currentTime;
				long elapsedTime;
				final long LOOP_TIME = 1000 / EscapeIR.FPS;

				while (true) {
					currentTime = System.currentTimeMillis();
					elapsedTime = currentTime - startingTime;
					startingTime = currentTime;

					//If the state is not stored nor instanciated, we put it in the hashmap
					final boolean newState = !this.states.containsKey(EscapeIR.currentState);
					switch (EscapeIR.currentState) {
					case QUIT:
						return;
					case GAME:
						if (newState)
							this.states.put(StateID.GAME, new GameState(context));
						break;
					case WIN:
						if (newState)
							this.states.put(StateID.WIN, new WinState(context));
					case LOOSE:
						if (newState)
							this.states.put(StateID.LOOSE, new LooseState(context));
					default:
						return;
					}

					//Update the state, giving it a delta time to compute movements
					this.states.get(EscapeIR.currentState).update(elapsedTime / (double) LOOP_TIME);
					//Display the state
					this.states.get(EscapeIR.currentState).render();
					//Sleep a computed time based on numbers of FPS
					EscapeIR.sleep((int) (startingTime + LOOP_TIME - System.currentTimeMillis()));
					//Clear the recognized gesture if any
					GestureRecognizer.getInstance().clearGesture();
				}
			}
		});
	}

	/**
	 * Switch between states
	 * @param state The state to switch to
	 */
	public static void stateSwitcher(StateID state) {
		EscapeIR.currentState = state;
	}

	/**
	 * Sleeps the current thread
	 * @param timeToSleep the amount of ms to sleep the thread
	 */
	public static void sleep(int timeToSleep) {
		if (timeToSleep < 0)
			timeToSleep = 0;
		try {
			Thread.sleep(timeToSleep);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
